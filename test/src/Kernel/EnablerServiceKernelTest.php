<?php

namespace Drupal\rsvplist\Kernel;

use Drupal;
use Drupal\Core\Test\TestDatabase;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\rsvplist\EnablerService;

/**
 * @coversDefaultClass \Drupal\rsvplist\EnablerService
 */
class EnablerServiceKernelTest extends EntityKernelTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  public static $modules = ['rsvplist', 'node'];

  /**
   * @var
   */
  protected $node;

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();

    $this->installConfig(['rsvplist']);
    $this->installSchema('rsvplist', ['rsvplist_enabled', 'rsvplist']);

    $user = $this->createUser();

    $container = Drupal::getContainer();
    $container->get('current_user')->setAccount($user);

    $this->node = Node::create([
      'title' => $this->getRandomGenerator()->word(10),
      'type' => 'page',
    ]);
    $this->node->save();
  }

  /**
   * @covers ::isEnabled
   */
  public function testIsEnabled() {
    $enabler = new EnablerService(TestDatabase::getConnection());
    $this->assertEquals(FALSE, $enabler->isEnabled($this->node));
  }

  /**
   * @covers ::setEnabled
   */
  public function testSetEnabled() {
    $enabler = new EnablerService(TestDatabase::getConnection());
    $enabler->setEnabled($this->node);
    $this->assertTrue($enabler->isEnabled($this->node));
  }

  /**
   * @covers ::delEnabled
   */
  public function testDelEnabled() {
    $enabler = new EnablerService(TestDatabase::getConnection());
    $enabler->delEnabled($this->node);
    $this->assertFalse($enabler->isEnabled($this->node));
  }

}
