<?php

namespace Drupal\rsvplist\Unit;

use Drupal\node\Entity\Node;
use Drupal\rsvplist\EnablerService;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\rsvplist\EnablerService
 *
 * @group rsvp
 */
class EnablerServiceTest extends UnitTestCase {

  protected $entityTypeId;

  /**
   * Mock a drupal node entity.
   *
   * @var Node $node ;
   */
  protected $node;

  /**
   * Mock a database connection.
   *
   * @var \Drupal\Core\Database\Database $database
   */
  protected $database;

  protected $select;

  protected $insert;

  protected $statement;

  /**
   * Test to make sure rsvp is enabled on a node.
   * @covers ::isEnabled
   */
  public function testIsEnabled() {
    $rsvpEnable = new EnablerService($this->database);
    $myNode = $this->prophesize(Node::class);
    $myNode->isNew()->willReturn(FALSE);
    $myNode->id()->willReturn($this->randomMachineName());
    $rsvpEnable->isEnabled($myNode->reveal());
    $this->assertFalse($rsvpEnable->isEnabled($myNode->reveal()));

    //    $rsvpEnable->isEnabled($this->node);
    //    var_dump($this->node);
  }

  /**
   * Test to set enabled for a node.
   * @covers ::setEnabled
   */
  public function testSetEnabled() {
    $rsvpEnable = new EnablerService($this->database);
    $rsvpEnable->setEnabled($this->node);
    $this->assertTrue($rsvpEnable->isEnabled($this->node));
  }

  /**
   * Test deleting enabled for a node.
   * @covers ::delEnabled
   */
    public function testDelEnabled() {
      $rsvpEnable = new EnablerService($this->database);
      $rsvpEnable->delEnabled($this->node);
      $this->assertTrue($rsvpEnable->isEnabled($this->node));
    }

  protected function setUp() {
    parent::setUp();
    $this->statement = $this
      ->getMockBuilder('Drupal\Core\Database\Driver\sqlite\Statement')
      ->disableOriginalConstructor()
      ->getMock();
    $this->statement
      ->expects($this->any())
      ->method('fetchObject')
      ->will($this->returnCallback([
        $this,
        'fetchObjectCallback',
      ]));

    $this->select = $this->getMockBuilder('Drupal\Core\Database\Query\Select')
      ->disableOriginalConstructor()
      ->getMock();
    $this->select->expects($this->any())
      ->method('fields')
      ->will($this->returnSelf());
    $this->select->expects($this->any())
      ->method('condition')
      ->will($this->returnSelf());
    $this->select->expects($this->any())
      ->method('execute')
      ->will($this->returnValue($this->statement));

    $this->insert = $this->getMockBuilder('Drupal\Core\Database\Query\Insert')
      ->disableOriginalConstructor()
      ->getMock();
    $this->insert->expects($this->any())
      ->method('fields')
      ->will($this->returnSelf());
    $this->insert->expects($this->any())
      ->method('execute')
      ->will($this->returnValue($this->statement));

    $this->database = $this->getMockBuilder('Drupal\Core\Database\Connection')
      ->disableOriginalConstructor()
      ->getMock();
    $this->database->expects($this->any())
      ->method('select')
      ->will($this->returnValue($this->select));
    $this->database->expects($this->any())
      ->method('insert')
      ->will($this->returnValue($this->insert));

    $this->node = $this->getMockBuilder(Node::class)
      ->disableOriginalConstructor()
      ->getMock();
    $this->node->method('isNew')->willReturn(FALSE);

  }
}
