<?php

namespace Drupal\rsvplist\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Component\Utility\EmailValidatorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an RSVP Email Form.
 */
class RSVPForm extends FormBase {

  /**
   * The Route Matching plugin.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The Entity type manager plugin.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $user;

  /**
   * The Current User plugin.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Email Validator plugin.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * Messenger service.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * RSVPForm constructor.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The Routematch plugin.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The Current User plugin.
   * @param \Drupal\Component\Utility\EmailValidatorInterface $emailValidator
   *   The Email validator plugin.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $user
   *   The Entity type magager interface for use in user.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   */
  public function __construct(
    RouteMatchInterface $routeMatch,
    AccountProxyInterface $currentUser,
    EmailValidatorInterface $emailValidator,
    EntityTypeManagerInterface $user,
    Messenger $messenger
  ) {
    $this->routeMatch = $routeMatch;
    $this->currentUser = $currentUser;
    $this->emailValidator = $emailValidator;
    $this->user = $user;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('current_route_match'),
      $container->get('current_user'),
      $container->get('email.validator'),
      $container->get('entity_type.manager'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'rsvplist_email_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $node = $this->routeMatch->getParameter('node');
    $user_mail = $this->currentUser->getEmail();
    $nid = $node->nid->value;
    $form['email'] = [
      '#title' => $this->t('Email Address'),
      '#type' => 'textfield',
      '#size' => 25,
      '#description' => $this->t(
        "We'll send updates to the email address you provide."
      ),
      '#required' => TRUE,
      '#default_value' => ($user_mail) ? $user_mail : '',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('RSVP'),
    ];
    $form['nid'] = [
      '#type' => 'hidden',
      '#value' => $nid,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $value = $form_state->getValue('email');
    if ($value == !$this->emailValidator->isValid($value)) {
      $form_state->setErrorByName(
        'email',
        $this->t('The email address %mail is not valid.', ['%mail' => $value])
      );
      return;
    }
    $node = $this->routeMatch->getParameter('node');
    // Check if email already is set for this node.
    $select = Database::getConnection()->select('rsvplist', 'r');
    $select->fields('r', ['nid']);
    $select->condition('nid', $node->id());
    $select->condition('mail', $value);
    $results = $select->execute();
    if (!empty($results->fetchCol())) {
      // We found a row with this nid and email.
      $form_state->setErrorByName(
        'email',
        $this->t(
          'The address %mail is already subscribed to this list.',
          ['%mail' => $value]
        )
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $user = $this->user->getStorage('user')->load($this->currentUser->id());
    $insert = Database::getConnection()->insert('rsvplist');
    $insert->fields(
      [
        'mail',
        'nid',
        'uid',
        'created',
      ],
      [
        $form_state->getValue('email'),
        $form_state->getValue('nid'),
        $user->id(),
        time(),
      ]
    );
    $insert->execute();
    $this->messenger()->addMessage(
      $this->t('Thank you for your RSVP, you are on the list for the event.')
    );
  }

}
