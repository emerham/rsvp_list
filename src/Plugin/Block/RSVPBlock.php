<?php

namespace Drupal\rsvplist\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\rsvplist\EnablerService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'RSVP' List Block.
 *
 * @Block(
 *   id = "rsvp_block",
 *   admin_label = @Translation("RSVP Block"),
 *   category = @Translation("Blocks")
 * )
 */
class RSVPBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The Route match plugin.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  private $routMatch;

  /**
   * The RSVP List Enabler.
   *
   * @var \Drupal\rsvplist\EnablerService
   */
  private $enabler;

  /**
   * The Form Builder plugin.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  private $formBuilder;

  /**
   * RSVPBlock constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The Plugin ID for the plugin instance.
   * @param string $plugin_definition
   *   The Plugin implementation definition.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The Route Match interface.
   * @param \Drupal\rsvplist\EnablerService $enabler
   *   The RSVP List Enabler service.
   * @param \Drupal\Core\Form\FormBuilderInterface $formBuilder
   *   The Form Builder Interface.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    RouteMatchInterface $routeMatch,
    EnablerService $enabler,
    FormBuilderInterface $formBuilder
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->routMatch = $routeMatch;
    $this->enabler = $enabler;
    $this->formBuilder = $formBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get(EnablerService::class),
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return $this->formBuilder->getForm('Drupal\rsvplist\Form\RSVPForm');
  }

  /**
   * {@inheritdoc}
   * */
  public function blockAccess(AccountInterface $account) {
    /** @var \Drupal\node\Entity\Node $node */
    $node = $this->routMatch->getParameter('node');
    if (!is_null($node)) {
      $nid = $node->nid->value;
      /** @var \Drupal\rsvplist\EnablerService $enable */
      if (is_numeric($nid)) {
        if ($this->enabler->isEnabled($node)) {
          return AccessResult::allowedIfHasPermission($account, 'view rsvplist');
        }
      }
    }
    return AccessResult::forbidden();
  }

}
